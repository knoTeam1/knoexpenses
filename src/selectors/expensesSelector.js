import { createSelector } from 'reselect';
import _ from 'lodash';

const monthExpensesSelector = state => state.monthExpenses;

const calcExpensesMonth = (expenses) => {
    let totalMonth = 0;    
    let expensesSort = _.map(expenses, (val, uid) => {
        return { ...val, uid };
    });   
    expensesSort = expensesSort.sort((a, b) => a.month - b.month || a.day - b.day);

    expensesSort.forEach((elem) => {
        if (elem.amount === undefined) {
            return;
        } if (elem.amount.includes(',')) {
            totalMonth += parseFloat(elem.amount.replace(',', '.'));
        } else {
            totalMonth += parseFloat(elem.amount);
        }
    });

    return { expensesSort, totalMonth };
};

export default createSelector(
    monthExpensesSelector,
    calcExpensesMonth
);
