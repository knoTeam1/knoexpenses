import { createSelector } from 'reselect';
import _ from 'lodash';

const expensesSelector = state => state.expenses;

const calcMonths = (expenses) => {
    const months = [
        { name: 'Enero', amount: 0 }, 
        { name: 'Febrero', amount: 0 }, 
        { name: 'Marzo', amount: 0 }, 
        { name: 'Abril', amount: 0 }, 
        { name: 'Mayo', amount: 0 }, 
        { name: 'Junio', amount: 0 }, 
        { name: 'Julio', amount: 0 }, 
        { name: 'Agosto', amount: 0 }, 
        { name: 'Septiembre', amount: 0 }, 
        { name: 'Octubre', amount: 0 }, 
        { name: 'Noviembre', amount: 0 }, 
        { name: 'Diciembre', amount: 0 }
    ];
    
    let expensesSort = _.map(expenses, (val, uid) => {
        return { ...val, uid };
    });   
    // expensesSort = expensesSort.sort((a, b) => a['month'] - b['month'] || a['day'] - b['day']);
    expensesSort = expensesSort.sort((a, b) => a.month - b.month || a.day - b.day);

    expensesSort.forEach((elem) => {
        months[elem.month - 1].amount += parseInt(elem.amount, 10);
        yearAmount += months[elem.month - 1].amount;
    });
    let yearAmount = 0;
    months.forEach((elem) => {
        yearAmount += elem.amount;
    });
    
    return { months, yearAmount };
};

export default createSelector(
    expensesSelector,
    calcMonths
);
