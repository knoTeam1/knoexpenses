import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

import { 
    EXPENSES_DATA_UPDATE,
    EXPENSES_FETCH_SUCCESS,
    EXPENSES_MONTH_FETCH_SUCCESS,
    EXPENSES_SAVE,
    EXPENSES_EDIT,
    EXPENSES_CLEAR_DATA,
    EXPENSES_DELETE
} from './types';


let currentGID = '-1';

//const month = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 
//'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

// checkIfExpensesExist = (gId, year) => {
//     //var expensesRef = firebase.database().ref(`/expenses/${gid}/`);
//     var expensesRef = firebase.database().ref('/expenses/1/');

//     expensesRef.child('2018').once('value', function(snapshot) {
//         var exists = (snapshot.val() !== null);
//         return exists;
//     });
// }

export const expensesDataUpdate = ({ prop, value }) => {
    return {
        type: EXPENSES_DATA_UPDATE,
        payload: { prop, value }
    };
};

export const expensesClearData = () => {
    console.log('clear data...');
    return {
        type: EXPENSES_CLEAR_DATA,
        payload: {}
    };
};

export const expensesSave = ({ name, amount, day, month, provider }) => {
    //console.log('save');
    //console.log({ name, type });
    //console.log('save expenses');
    //const gid = '1';
    const gid = currentGID;
    // const cyear = '2017';
    const cyear = new Date().getFullYear();
    amount = amount.replace(',', '.');

    return (dispatch) => {
        firebase.database().ref(`/expenses/${gid}/${cyear}/`)
            .push({ amount, day, month, name, provider })
            .catch(console.log('error'))
            .then(() => {
                dispatch({ type: EXPENSES_SAVE });
                Actions.pop({ type: 'reset ' });
            });
    };
};

export const expensesEdit = ({ name, amount, day, month, provider, eid }) => {
    //const gid = '1';
    const gid = currentGID;
    // const cyear = '2017';
    const cyear = new Date().getFullYear();
    amount = amount.replace(',', '.');

    return (dispatch) => {
        firebase.database().ref(`/expenses/${gid}/${cyear}/${eid}`)
            .set({ name, amount, day, month, provider })
            .then(() => {
                dispatch({ type: EXPENSES_EDIT });
                Actions.pop({ type: 'reset ' });
            });
    };
};

// export const expensesFetch = () => {
//     //console.log('get expenses');
//     const gid = '1';
//     // const cyear = '2017';
//     const cyear = new Date().getFullYear();
    
//     return (dispatch) => {
//         firebase.database().ref(`/expenses/${gid}/${cyear}/`)
//             .on('value', snapshot => {
//                  console.log(snapshot.val());
//                  dispatch({ type: EXPENSES_FETCH_SUCCESS, payload: snapshot.val() });    
//             });
//     };
// };


export const yearExpensesToFetch = (gid, year) => {
    //console.log('yearExpensesToFetch');
    currentGID = gid;
    //const gid = '1';
    //const cyear = '2017';
    return (dispatch) => {
        firebase.database().ref(`/expenses/${gid}/${year}/`)
            .orderByChild('month')
            .on('value', snapshot => {
                 //console.log(snapshot.val());
                 dispatch({ type: EXPENSES_FETCH_SUCCESS, payload: snapshot.val() });    
            });
    };
};

export const monthExpensesToFetch = (gid, year, month) => {
    console.log('monthlyExpensesToFetch');
    currentGID = gid;
    //console.log(year);
    //console.log(month);
    //const gid = '1';
    //const cyear = year;    
    //const currMonth = month;
    //currMonth = currMonth.toString();
    //month[new Date().getMonth()];
    //console.log(currMonth);
    return (dispatch) => {
          firebase.database().ref(`/expenses/${gid}/${year}/`)
              .orderByChild('month')
              .equalTo(month)
              .on('value', snapshot => {
                   //console.log(snapshot.val());
                   dispatch({ type: EXPENSES_MONTH_FETCH_SUCCESS, payload: snapshot.val() });    
              });
      };
};

// export const currentExpensesFetch = () => {
//     //console.log('get current expenses');
//     const gid = '1';
//     // const cyear = '2017';
//     const cyear = new Date().getFullYear();
    
//     const currMonth = new Date().getMonth() + 1;
//     //currMonth = currMonth.toString();
//     //month[new Date().getMonth()];
//     //console.log(currMonth);
//     return (dispatch) => {
//         firebase.database().ref(`/expenses/${gid}/${cyear}/`)
//             .orderByChild('month')
//             .equalTo(currMonth.toString())
//             .on('value', snapshot => {
//                  //console.log(snapshot.val());
//                  dispatch({ type: EXPENSES_FETCH_SUCCESS, payload: snapshot.val() });    
//             });
//     };
// };

export const expensesDelete = ({ eid }) => {
    //const { currentUser } = firebase.auth();
    //const gid = '1';
    const gid = currentGID;
    // const cyear = '2017';
    const cyear = new Date().getFullYear();
    

    return () => {
      //  firebase.database().ref(`/users/${currentUser.uid}/employees/${eid}`)
      firebase.database().ref(`/expenses/${gid}/${cyear}/${eid}`)
            .remove()    
            .then(() => {
                Actions.pop({ type: 'reset ' });
            });
    };
};
