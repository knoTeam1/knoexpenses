import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

import { 
    EMAIL_CHANGED, 
    PASSWORD_CHANGED, 
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    LOGIN_USER,
    LOGOUT_USER,
    GLOBALS_SET_GID,
} from './types';

///// LOGIN FORM

export const emailChanged = (text) => {
    return {
        type: EMAIL_CHANGED,
        payload: text
    };
};

export const passwordChanged = (text) => {
    return {
        type: PASSWORD_CHANGED,
        payload: text
    };
};

export const logoutUser = () => {
    return (dispatch) => {
        firebase.auth().signOut()
            .then(() => { 
                dispatch({ type: LOGOUT_USER });
                Actions.auth(); 
            });
    };
};

export const loginUser = ({ email, password }) => {
    return (dispatch) => {
        dispatch({ type: LOGIN_USER });

    firebase.auth().signInWithEmailAndPassword(email, password)
        .then(user => loginUserSuccess(dispatch, user))
        // .catch(error => {
        //     console.log('catch auth');
        //     console.log(error);
        //     loginUserFail(dispatch);
        //     // firebase.auth().createUserWithEmailAndPassword(email, password)
        //     //     .then(user => loginUserSuccess(dispatch, user))
        //     //     .catch(() => loginUserFail(dispatch));
        // });
        .catch(error => {
            if (error.code === 'auth/wrong-password' ||
                 error.code === 'auth/invalid-email' ||
                 error.code === 'auth/user-disabled' ||
                 error.code === 'auth/user-not-found') {
                    console.log('catch auth');
                    console.log(error);
                    loginUserFail(dispatch, error.code);
            }
        });
    };
};

const loginUserSuccess = (dispatch, user) => {

    dispatch({ 
        type: LOGIN_USER_SUCCESS, 
        payload: user 
    }); 
    getUserGID(dispatch, user.uid);

    //Change scene
    //console.log('loginUserSuccess');
    
};

const loginUserFail = (dispatch, errCode) => {
    console.log('loginUserFailed');
    dispatch({ 
        type: LOGIN_USER_FAIL,
        payload: errCode
    }); 
    setGID(dispatch, '-1');
};

export const setGID = (dispatch, gid) => {
    dispatch({ 
            type: GLOBALS_SET_GID,
            payload: gid
     });
     if (gid > 0) {
        Actions.main();
     }
};

const getUserGID = (dispatch, uid) => {
    let gid = -1;
    firebase.database().ref('/userGroups/').on('value', snapshot => {
            snapshot.val().forEach((element, i) => {
                if (_.has(element, uid)) {
                    gid = i;        
                }
            });
            setGID(dispatch, gid);
    });
};
