export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';
export const LOGOUT_USER = 'logout_user';

// PROVIDERS
export const PR_DATA_UPDATE = 'provider_data_updated';
export const PR_FETCH_SUCCESS = 'provider_fetch_success';
export const PR_SAVE = 'provider_save';
export const PR_EDIT = 'provider_edit';

// EXPENSES
export const EXPENSES_DATA_UPDATE = 'expenses_data_upate';
export const EXPENSES_CLEAR_DATA = 'expenses_clear_data';
export const EXPENSES_FETCH_SUCCESS = 'expenses_fetch_success';
export const EXPENSES_MONTH_FETCH_SUCCESS = 'expenses_month_fetch_success';
export const EXPENSES_SAVE = 'expenses_save';
export const EXPENSES_EDIT = 'expenses_edit';
export const EXPENSES_DELETE = 'expenses_delete';

//GLOBAL VARS
export const GLOBALS_SET_GID = 'globals_set_gid';
