import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

import { 
    PR_DATA_UPDATE,
    PR_FETCH_SUCCESS,
    PR_SAVE,
    PR_EDIT
} from './types';

let currentGID = '-1';

export const providerDataUpdate = ({ prop, value }) => {
    return {
        type: PR_DATA_UPDATE,
        payload: { prop, value }
    };
};

export const providerSave = ({ name, type }) => {
    //console.log('save');
    //console.log({ name, type });
    return (dispatch) => {
        firebase.database().ref(`/providers/${currentGID}`)
            .push({ name, type })
            .catch(console.log('error'))
            .then(() => {
                dispatch({ type: PR_SAVE });
                Actions.pop({ type: 'reset ' });
            });
    };
};

export const providerEdit = ({ name, type, pid }) => {
    return (dispatch) => {
        firebase.database().ref(`/providers/${currentGID}/${pid}`)
            .set({ name, type })
            .then(() => {
                dispatch({ type: PR_EDIT });
                Actions.pop({ type: 'reset ' });
            });
    };
};

export const providersFetch = (gid) => {
    console.log('get providers');
    currentGID = gid;
    return (dispatch) => {
        firebase.database().ref(`/providers/${currentGID}`)
            .on('value', snapshot => {
                 console.log(snapshot.val());
                 dispatch({ type: PR_FETCH_SUCCESS, payload: snapshot.val() });    
            });
    };
};
