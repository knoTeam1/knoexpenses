import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { providersFetch, logoutUser } from '../actions/';
import DsgnMenuButton from '../components/common/designMenuButton';

class HomeForm extends Component {

    componentWillMount() {        
        //console.log('HOME WILL MOUNT');
        //console.log(this.props.globals.gid);
        if (this.props.globals.gid > 0) {
            this.props.providersFetch(this.props.globals.gid);
        }
    }
    
    onLogoutPress() {
        this.props.logoutUser();
    }

    render() {       
        const { mainStyle, headerStyle, menuRowStyle, 
                imgStyle, buttonStyle1, buttonStyle2, 
                buttonStyle3, buttonStyle4 
        } = styles;

        return (   
            <View style={mainStyle}> 
                <View style={headerStyle}> 
                    <Image
                        style={imgStyle}  
                        source={require('../../assets/images/logoMenu.png')}
                    /> 
                </View>
                <View style={menuRowStyle}> 
                    <DsgnMenuButton 
                        text='ESTE MES' 
                        buttonStyle={buttonStyle1} 
                        img={'icon1'}
                        onPress={() => { Actions.appMonthExpenses({ showMonth: 0, showYear: 0 }); }}
                    />
                    <DsgnMenuButton 
                        text='MENSUALES' 
                        buttonStyle={buttonStyle3} 
                        img={'icon2'}
                        onPress={() => { Actions.appMonthSelector(); }} 
                    />
                </View>
                <View style={menuRowStyle}> 
                    <DsgnMenuButton 
                        text='ANUALES' 
                        buttonStyle={buttonStyle1} 
                        img={'icon3'}
                        onPress={() => { Actions.appExpenses(); }} 
                    />
                    <DsgnMenuButton 
                        text='PROVEEDORES' 
                        buttonStyle={buttonStyle3} 
                        img={'icon4'}
                        onPress={() => { Actions.appProviders(); }} 
                    />
                </View>
                <View style={menuRowStyle}> 
                    <DsgnMenuButton 
                        text='LOGOUT' 
                        buttonStyle={buttonStyle4} 
                        img={'icon5'}
                        onPress={this.onLogoutPress.bind(this)} 
                    />
                    {/* <DsgnMenuButton 
                        text='' 
                        buttonStyle={buttonStyle2} 
                    /> */}
                </View>
            </View>    
        );
    }
}

const styles = {
    mainStyle: {
        flex: 1, 
        flexDirection: 'column'
      },
    headerStyle: {
        //flex: 1, 
        height: 125,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#283947'
      },
    menuRowStyle: {
        flex: 1,
        flexDirection: 'row',
        //alignItems: 'center',
        //justifyContent: 'center',
        //alignItems: 'center',
        //height: 200,
        backgroundColor: '#273947',
      },   
      buttonStyle1: {
        borderRightWidth: 0,
        borderBottomWidth: 0,
      },
      buttonStyle2: {
        borderWidth: 1,
      },
      buttonStyle3: {
        borderBottomWidth: 0,
      },
      buttonStyle4: {
        borderRightWidth: 0,
      },
    imgStyle: {       
        height: 100,
        width: 100
    }
};


const mapStateToProps = state => {
    const { globals } = state;
    return { globals };
};

export default connect(mapStateToProps, { 
    providersFetch,
    logoutUser
})(HomeForm);

