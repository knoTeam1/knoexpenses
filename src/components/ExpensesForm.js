import React, { Component } from 'react';
import _ from 'lodash';
import { Picker, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { 
    expensesDataUpdate,
} from '../actions';
import { Input, CardSection } from './common';

class ExpensesForm extends Component {
    
    componentWillMount() {
       // _.each(this.props.employee, (value, props) => {
       //     console.log({ props, value });
       //     this.props.expensesDataUpdate({ props, value });
       // });
    }

    render() {    
        const providerItems = _.map(this.props.providers, (s, i) => {
            return <Picker.Item key={i} value={i} label={s.name} />;
        });
        
        return (
            <View>
                <CardSection>
                    <Input
                        label="Nombre"
                        onChangeText={text => 
                            this.props.expensesDataUpdate({ prop: 'name', value: text })
                        }
                        value={this.props.name}
                    />
                </CardSection>
                <CardSection>
                    <Input
                        label="Importe"
                        onChangeText={text => 
                            this.props.expensesDataUpdate({ prop: 'amount', value: text })
                        }
                        value={this.props.amount}
                    />
                </CardSection>
                <CardSection>
                    <Text>Proveedor</Text>
                    <Picker
                        style={{ flex: 1 }}
                        selectedValue={this.props.provider}
                        onValueChange={value => 
                            this.props.expensesDataUpdate({ prop: 'provider', value })}
                    >
                        {providerItems}
                    </Picker>
                </CardSection>
                <CardSection>
                    <Input
                        label="Día"
                        onChangeText={text => 
                            this.props.expensesDataUpdate({ prop: 'day', value: text })
                        }
                        value={this.props.day}
                    />
                </CardSection>
                <CardSection >
                    <Text style={styles.pickerTextStyle}>Mes: </Text>
                    <Picker
                        style={{ flex: 1 }}
                        selectedValue={this.props.month}
                        onValueChange={value => 
                            this.props.expensesDataUpdate({ prop: 'month', value })}
                    >
                            <Picker.Item label="Enero" value='1' />
                            <Picker.Item label="Febrero" value='2' />
                            <Picker.Item label="Marzo" value='3' />
                            <Picker.Item label="Abril" value='4' />
                            <Picker.Item label="Mayo" value='5' />
                            <Picker.Item label="Junio" value='6' />
                            <Picker.Item label="Julio" value='7' />
                            <Picker.Item label="Agosto" value='8' />
                            <Picker.Item label="Septiembre" value='9' />
                            <Picker.Item label="Octubre" value='10' />
                            <Picker.Item label="Noviembre" value='11' />
                            <Picker.Item label="Diciembre" value='12' />
                    </Picker>                    
                </CardSection>       
            </View>         
        );
    }
}

const styles = {
    pickerTextStyle: {
        fontSize: 18,
        paddingLeft: 20,
        paddingRight: 40
    }
};

const mapStateToProps = (state) => {
    const { name, amount, day, month, provider } = state.expense;
    const { providers } = state;
    //console.log('FORM');
    //console.log(providers);
    return { name, amount, day, month, provider, providers };
};

export default connect(mapStateToProps, { 
    expensesDataUpdate
})(ExpensesForm);
