import React, { Component } from 'react';
//import _ from 'lodash';
import { Text, TouchableWithoutFeedback, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { CardSection } from '../common';

class ListItemExpensesDetail extends Component {

    onRowPress() {
        //console.log('Check expense :' + this.props.provider);
        Actions.appExpensesEdit({ expense: this.props.provider });
    }

    renderSeparator = () => {
        return (
          <View
            style={{
              height: 100,
              width: 2,
              backgroundColor: '#D81458'
            }}
          />
        )
      }

    parseNumToLocaleStr(num){
        if ( num === undefined) return;
        return num.toLocaleString('es-ES', { maximumFractionDigits: 2 }) + ' €';
    }

    render() {
        const { name, day, amount } = this.props.provider;
        const { titleStyle, dayStyle, amountStyle, providerStyle, containerDayStyle, 
            containerStyle, containerAmountStyle, containerMiddleStyle } = styles;

        return (
            <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
                <View>
                    <CardSection>
                        <View style={containerDayStyle}>
                            <Text style={dayStyle}>
                                {day}
                            </Text>
                        </View>
                        {/* verticalLine = { this.renderSeparator } */}
                        <View style={containerMiddleStyle}>
                            <Text style={titleStyle}>
                                {name}
                            </Text>
                            <Text style={providerStyle}>
                                { this.props.providerName }
                            </Text>
                        </View>
                        <View style={containerAmountStyle}>
                            <Text style={amountStyle}>
                                {this.parseNumToLocaleStr(parseFloat(amount))}
                            </Text>
                        </View>
                    </CardSection>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = {
    containerStyle: {
        //flex: 1,
        //backgroundColor: '#ffff00'
    },
    containerDayStyle: {
        //flex: 1,
        justifyContent: 'center',
        width: 75,
        //backgroundColor: '#ffff00'
    },
    containerAmountStyle: {
        flex: 1,
        //backgroundColor: '#ff00ff'
    },
    containerMiddleStyle: {
        flex: 1,
        //backgroundColor: '#f1001f'
    },
    titleStyle: {
        //backgroundColor: '#ff0000',
        color: '#000000',
        fontSize: 18        
    },
    dayStyle: {
        fontSize: 26,
        //backgroundColor: '#00ff00',
        color: 'blue',
        textAlign: 'center',
        paddingLeft: 15,
        paddingRight: 15
    },
    amountStyle: {
        fontSize: 26,
        alignSelf: 'flex-end',
        //backgroundColor: '#0000ff',
        textAlign: 'right',
        color: 'red',
        paddingLeft: 15
    },
    providerStyle: {
        fontSize: 14,
        textAlignVertical: 'bottom'
    }
};

export default ListItemExpensesDetail;
