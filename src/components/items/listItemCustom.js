import React, { Component } from 'react';
import _ from 'lodash';
import { TouchableWithoutFeedback, View, Image } from 'react-native';
import { Icon, CardItem, Text, Left,  Right } from 'native-base';


class ListItemCustom extends Component {
    renderLeftIcon() {
        const hasIcon = _.has(this.props, 'leftIcon');
        if (hasIcon) {
           return <Icon name="arrow-forward" />;
        } else {
            return '';
        }
    };

    // renderText1(){
        
    // }

    // renderText2(){
        
    // }

    // renderText3(){
        
    // }

    // renderRightIcon(){
        
    // }
    render() {
        const { text1, text2, onPress } = this.props;
        const { rightImageStyle, containerStyle, text1Style, text2Style } = styles;
    
        return (
                <TouchableWithoutFeedback onPress={onPress}>
                    <View style={containerStyle}>
                        <Text style={text1Style}>{text1}</Text>
                        <Text style={text2Style}>{text2}</Text>
                        <View style={rightImageStyle}>
                            <Image 
                                source={require('../../../assets/images/rightArrow.png')} 
                                style={{ width: 12, height: 15 }} 
                            /> 
                        </View>
                    </View>
                </TouchableWithoutFeedback>
        );
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        flexDirection: 'row',
        height: 70,
        backgroundColor: '#FFFFFF',
        borderBottomWidth: 1,
        borderColor: '#F2F5F4',
        alignItems: 'center'
    },
    text1Style: {
        fontSize: 22,
        color: '#273846',
        textAlign: 'left',
        flexGrow: 1,
        paddingLeft: 20
    },
    text2Style: {
        fontSize: 22,
        color: '#FA673A',
        textAlign: 'right',
        paddingRight: 20
    },
    rightImageStyle: {
        paddingTop: 3,
        paddingRight: 20,
    }
};

export default ListItemCustom;
