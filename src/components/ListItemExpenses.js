import React, { Component } from 'react';
import _ from 'lodash';
import { Text, TouchableWithoutFeedback, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { CardSection } from './common';

class ListItem extends Component {

    onRowPress() {
        //console.log('Check expense :' + this.props.provider);
        Actions.appExpensesEdit({ expense: this.props.provider });
    }

    drawMonth() {
        // console.log(this.props.drawMonth);
        const hasMonth = _.has(this.props, 'drawMonth');
        if (hasMonth !== undefined) {
            return (<Text style={styles.titleStyle}>
                {this.props.drawMonth}
            </Text>); 
        }
    }

    render() {
        //console.log('ListItemExpenses:');
        //console.log(this.props.provider);
        const { name, day } = this.props.provider;

        return (
            <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
                <View>
                    {this.drawMonth()}
                    <CardSection>
                        <Text style={styles.titleStyle}>
                            {day} - {name}
                        </Text>
                    </CardSection>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = {
    titleStyle: {
        fontSize: 18,
        paddingLeft: 15
    }
};

export default ListItem;
