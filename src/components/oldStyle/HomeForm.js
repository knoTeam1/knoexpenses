import React, { Component } from 'react';
import { View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { providersFetch, logoutUser } from '../actions/';
import { Container, Header, Left, Body, Button, Icon, Title, Right } from 'native-base';

import { ImgButton } from './common/';


class HomeForm extends Component {

    componentWillMount() {        
        console.log('HOME WILL MOUNT');
        console.log(this.props.globals.gid);
        if (this.props.globals.gid > 0)
            this.props.providersFetch(this.props.globals.gid);
    }
    
    onLogoutPress() {
        this.props.logoutUser();
    }

    render() {       
        const { headerStyle, menuContainer } = styles;

        return (       
            <Container>
                <Header style={headerStyle}> 
                    <Left>
                        <Button transparent>
                                <Icon name='logo-bitcoin' />
                        </Button> 
                    </Left>           
                    <Body>
                        <Title>- knoExpenses - </Title>
                    </Body>  
                    <Right>
                        <Button transparent>
                                <Icon name='logo-bitcoin' />
                        </Button> 
                    </Right>    
                </Header>
                <View style={menuContainer}>      
                        
                        <ImgButton 
                            imgSource={'menuToday'} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 0, showYear: 0 }); }} 
                        > 
                            ESTE MES                 
                        </ImgButton>
                        <ImgButton 
                            imgSource={'menuToday'} 
                            onPress={() => { Actions.appMonthSelector(); }} 
                        > 
                            MENSUALES                 
                        </ImgButton>
                        <ImgButton 
                            imgSource={'menuToday'} 
                            onPress={() => { Actions.appExpenses(); }} 
                        > 
                            ANUALES                 
                        </ImgButton>
                        <ImgButton 
                            imgSource={'menuToday'} 
                            onPress={() => { Actions.appProviders(); }} 
                        > 
                            PROVEEDORES                 
                        </ImgButton>
                        <ImgButton 
                            imgSource={'menuToday'} 
                            onPress={this.onLogoutPress.bind(this)} 
                        > 
                            LOGOUT                 
                        </ImgButton>
                </View>
                
            </Container>
        );
    }
}

const styles = {
    headerStyle: {
        backgroundColor: '#0e1012'
    },
    // contentStyle: {
    //     borderWidth: 2,
    //     borderRadius: 0,
    //     borderColor: '#2d3034',
    //     backgroundColor: '#d84949'
    // },
    // menuItem: {
    //     //flex: 1,  
    //     backgroundColor: 'steelblue',
    //     padding: 5,
    //     width: 150,
    //     height: 150,
    //     borderWidth: 2,
    //     borderColor: '#ffffff', 
    //     // lineColor: '#000000',S
    //     // marginTop: '10px',
    //     // lineHeight: '150px',
    //     //  fontWeight: 'bold',
    //     //  fontSize: '3em',
    //     textAlign: 'center'
    // },
    menuContainer: {
        
        display: 'flex',
        flex: 1,
        flexDirection: 'row', 
        flexWrap: 'wrap', 
        // padding: 0,
        // margin: 0
        justifyContent: 'space-around',
        alignContent: 'space-around',
        backgroundColor: '#d84949'
    }
};


const mapStateToProps = state => {
    const { globals } = state;
    return { globals };
};

export default connect(mapStateToProps, { 
    providersFetch,
    logoutUser
})(HomeForm);

