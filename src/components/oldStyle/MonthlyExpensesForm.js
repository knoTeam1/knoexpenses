import React, { Component } from 'react';
import { ListView } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Drawer, Header, Title, Container, Button, Icon, Content, 
    Text, Left, Body, Right } from 'native-base';
import SideBar from './SideBar';
import { 
    monthExpensesToFetch,
    providersFetch 
} from '../actions/';
import ListItemExpensesDetail from './ListItemExpensesDetail';
import { Card, CardSection, Spinner } from './common/';

let totalAmount;
const month = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 
'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

let currMonth;

class MonthForm extends Component {

    state = { dataLoaded: false } ;

    componentWillMount() {        
        let m;
        let y;
        const hasMonth = _.has(Actions.currentParams, 'showMonth');
        const hasYear = _.has(Actions.currentParams, 'showYear');
        this.setState({ dataLoaded: false });
        this.props.providersFetch(this.props.globals.gid);
        // console.log('monthsWillMount');
        if (hasMonth && hasYear) {
            m = Actions.currentParams.showMonth;    
            y = Actions.currentParams.showYear;
            if (m === 0 && y === 0) {
                m = new Date().getMonth();
                y = new Date().getFullYear();
                //y = 2017;
                currMonth = month[m];     
                m += 1;
            } else {
                currMonth = month[m - 1];  
            }

            this.props.monthExpensesToFetch(this.props.globals.gid, String(y), String(m));
            this.createDataSource(this.props);
        } 
    }
    
    componentWillReceiveProps(nextProps) {
        //nextProps next set of props will be rendrered and
        //this.props is the old set
        this.createDataSource(nextProps);
        //console.log('nextProps');
        //console.log(nextProps.expenses);
        totalAmount = 0;
        //console.log('monthsWillReceiveProps');
        nextProps.expenses.forEach((elem) => {
            //console.log(elem);
            if (elem.amount === undefined) {
                return;
            } if (elem.amount.includes(',')) {
                totalAmount += parseFloat(elem.amount.replace(',', '.'));
            } else {
                totalAmount += parseFloat(elem.amount);
            }
        });
        this.setState({ dataLoaded: true });
    }

    getProviderName(pId) {
        //console.log(this.props.providers);
        return this.props.providers[pId].name;
    }

    createDataSource({ expenses }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(expenses);
    }

    renderHomeRow(expense) {     
        totalAmount += expense.amount;

        //console.log('renderHome');
        //console.log(this.props.providers);
        let pName;
        if (this.props.providers[expense.provider] === undefined) {
            pName = '';
        } else {
            pName = this.props.providers[expense.provider].name;
        }   

        return <ListItemExpensesDetail 
            provider={expense} 
            providerName={pName} 
        />;
    }

    renderContent() {     
        const { contentStyle, monthStyle, amountStyle, cardStyle } = styles;
        switch (this.state.dataLoaded) {
            case true:
                return (
                    <Content style={contentStyle}>
                    <Card>
                        <CardSection style={cardStyle}>
                            <Text style={monthStyle}>
                                {currMonth}
                            </Text>
                        </CardSection>
                        <CardSection style={cardStyle}>
                            <Text style={amountStyle}>
                                {this.parseNumToLocaleStr(totalAmount)}
                            </Text>
                        </CardSection>
                    </Card>
                    <Card>
                        <CardSection>
                            <ListView
                                enableEmptySections
                                dataSource={this.dataSource}
                                renderRow={this.renderHomeRow.bind(this)}
                            />
                        </CardSection>
                    </Card>
                    </Content>
                );
            case false:
            default:
                return (<Spinner size="large" />);      
        }    
    }

    parseNumToLocaleStr(num) {
        if (num === undefined) return;
        return num.toLocaleString(undefined, { maximumFractionDigits: 2 }) + ' €';
    }

    render() {
        const { headerStyle } = styles;
                
        return (       
            // <Drawer
            //     ref={(ref) => { this.drawer = ref; }}
            //     content={<SideBar navigator={this.navigator} />}
            //     onClose={() => this.closeDrawer()} 
            // >
            <Container style={{ flex: 1 }}>
                <Header style={headerStyle}>  
                    <Left>
                        <Button transparent onPress={() => Actions.pop({ type: 'reset ' })}>
                            <Icon name='md-arrow-round-back' />
                        </Button>
                    </Left>           
                    <Body>
                        <Title>Gastos</Title>
                    </Body>
                    {/* <Right>
                        <Button transparent onPress={() => this.openDrawer()}>
                            <Icon name='md-menu' />
                        </Button>
                    </Right> */}
                </Header> 
                {this.renderContent()}    
            </Container>
            // </Drawer>
        );
    }
}

const styles = {
    cardStyle: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerStyle: {
        backgroundColor: '#000000'
    },
    monthStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 24,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10

    },
    amountStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 48,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    },
    contentStyle: {
        borderWidth: 2,
        borderRadius: 0,
        borderColor: '#2d3034',
        backgroundColor: '#d84949'
    }
};

const mapStateToProps = state => {
    //CONVERT state.providers object to employees array, expected
    //by datasource
    
    console.log('month mapStateToProps');
    console.log(state);
    
    let expenses = _.map(state.monthExpenses, (val, uid) => {
         return { ...val, uid };
     });
     expenses = expenses.sort((a, b) => parseInt(a.day, 10) - parseInt(b.day, 10));
    //console.log('monthForm');
    //console.log(state);
    const { providers, globals } = state;

    return { expenses, providers, globals };
};

export default connect(mapStateToProps, { 
    monthExpensesToFetch,
    providersFetch
})(MonthForm);
