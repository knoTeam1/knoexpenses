import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import { connect } from 'react-redux';
import { ListView, View } from 'react-native';
import { Drawer, Container, Content, Header, Left, Button, 
    Body, Right, Icon, Title, List, Text } from 'native-base';
import ListItem from './ListItemExpenses';
import SideBar from './SideBar';
import { yearExpensesToFetch } from '../actions/';
import { Card, CardSection, Input } from './common/';

let lastMonth = '0';
let yearAmount;

let months = [
    { name: 'Enero', amount: 0 }, 
    { name: 'Febrero', amount: 0 }, 
    { name: 'Marzo', amount: 0 }, 
    { name: 'Abril', amount: 0 }, 
    { name: 'Mayo', amount: 0 }, 
    { name: 'Junio', amount: 0 }, 
    { name: 'Julio', amount: 0 }, 
    { name: 'Agosto', amount: 0 }, 
    { name: 'Septiembre', amount: 0 }, 
    { name: 'Octubre', amount: 0 }, 
    { name: 'Noviembre', amount: 0 }, 
    { name: 'Diciembre', amount: 0 }
];


class ExpensesListForm extends Component {

    state = { currYear: '0' } 
    

    closeDrawer() {
        this.drawer._root.close();
    }

    openDrawer() {
        this.drawer._root.open();
    }

    componentWillMount() {
        this.setState({ currYear: new Date().getFullYear() }); 
        this.props.yearExpensesToFetch(new Date().getFullYear());
        this.createDataSource(this.props);
    }

    resetMonthsAmount() {
        months.forEach((elem) => {
            elem.amount = 0;
        });   
    }

    componentWillReceiveProps(nextProps) {
        //nextProps next set of props will be rendrered and
        //this.props is the old set
        this.createDataSource(nextProps);
        
        yearAmount = 0;
        this.resetMonthsAmount();
        nextProps.expenses.forEach((elem) => {
            months[elem.month - 1].amount += parseInt(elem.amount, 10);
            yearAmount += parseInt(elem.amount, 10);
        });
    
    }

    createDataSource({ expenses }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(expenses);
    }

    renderRow(expense) {
        //console.log('RENDER LIST FORM');
        //console.log(expense);
        //console.log(lastMonth);
        //console.log(expense.month);
        if (parseInt(lastMonth, 10) === parseInt(expense.month, 10)) {
            return <ListItem provider={expense} />;
        } else {
             //console.log(expense.month);
             lastMonth = expense.month;
             //return [<Text>{month[lastMonth - 1]}</Text>, <ListItem provider={expense} />];
             return <ListItem provider={expense} drawMonth={months[lastMonth - 1].name} />;
         }
    }

    selectYear(text) {
        //console.log('select year');
        //console.log(text);
        this.setState({ currYear: text }, () => {
            //if (this.state.currYear >= 2017) {
                this.props.yearExpensesToFetch(this.state.currYear);  
            //}        
        });
    }

    render() {
        const { yearStyle, amountStyle, cardStyle, headerStyle } = styles;
        return (
            <Drawer
            ref={(ref) => { this.drawer = ref; }}
            content={<SideBar navigator={this.navigator} />}
            onClose={() => this.closeDrawer()} 
            >
            <Container>  
                <Header style={headerStyle}>  
                    <Left>
                        <Button transparent onPress={() => Actions.pop({ type: 'reset ' })}>
                            <Icon name='md-arrow-round-back' />
                        </Button>
                    </Left>
                              
                    <Body>
                        <Title>Gastos Anuales</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => Actions.appExpensesCreate()}>
                            <Icon name='md-add' />
                        </Button>
                    </Right> 
                </Header> 
                <Content>
                    {/* <Input
                        label="Año"
                        onChangeText={this.selectYear.bind(this)}
                        value={this.state.currYear.toString(10)}
                    /> */}
                    <Card>
                        <CardSection style={cardStyle}>
                            <Text style={yearStyle}>
                                {new Date().getFullYear()}
                            </Text>
                        </CardSection>
                        <CardSection style={cardStyle}>
                            <Text style={amountStyle}>
                                {yearAmount}€
                            </Text>
                        </CardSection>
                    </Card>
                    <View style={{ flex: 1 }}>
                        <ListView
                            enableEmptySections
                            dataSource={this.dataSource}
                            renderRow={this.renderRow.bind(this)}
                        />
                    </View>
                </Content>
            </Container>
        </Drawer>
        );
    }
}

function orderByProperty(prop) {
    let args = Array.prototype.slice.call(arguments, 1);
    return function (a, b) {
      let equality = a[prop] - b[prop];
      if (equality === 0 && arguments.length > 1) {
        return orderByProperty.apply(null, args)(a, b);
      }
      return equality;
    };
}

const styles = {
    headerStyle: {
        backgroundColor: '#000000'
    },
    cardStyle: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    yearStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 24,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10

    },
    amountStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 48,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    }
};

const mapStateToProps = state => {
    //CONVERT state.providers object to employees array, expected
    //by datasource
    //console.log(state.providers);
    //console.log('maptopropsLF');
    let expenses = _.map(state.expenses, (val, uid) => {
        return { ...val, uid };
    });
    
     expenses = expenses.sort((a, b) => a['month'] - b['month'] || a['day'] - b['day']);
     //expenses = expenses.sort(orderByProperty('month', 'day'));
     //console.log('expenses list form');
     //console.log(state.expenses);
     //console.log('expenses sorted');
     //console.log(expenses);
    return { expenses };
};

export default connect(mapStateToProps, { yearExpensesToFetch })(ExpensesListForm);
