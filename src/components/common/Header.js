import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import {
    Text,
    TouchableHighlight,
    View,
    Image
  } from 'react-native';

class Header extends Component {
    
    renderLeft() {
        const { imgStyle, menuLeft } = styles;
        if (this.props.back) {
            return (
                <TouchableHighlight onPress={() => Actions.pop({ type: 'reset ' })}>
                    <View style={menuLeft}>
                        <Image
                            style={imgStyle}  
                            source={require('../../../assets/images/icons/icons8-simple-arrow.png')}
                        /> 
                    </View>
                </TouchableHighlight>
            );
        }
    }

    renderTitle() {
        const { textStyle } = styles;
        return (
            <Text style={textStyle}>{this.props.title}</Text>
        );
    }

    renderRight() {
        const { imgStyle, menuRight, menuRightContent } = styles;   
        if (this.props.add) {
            return (
                <TouchableHighlight onPress={this.props.onAddPress}>
                  <View style={menuRight}>
                    <View style={menuRightContent}>
                        <Image
                            style={imgStyle}  
                            source={require('../../../assets/images/icons/icons8-add.png')}
                        />
                    </View>
                  </View> 
                 </TouchableHighlight>
            );
        }
    }

    render() {
        const { headerStyle } = styles;
        return (
            <View style={[headerStyle, this.props.headerStyle]}>
                {this.renderLeft()}
                {this.renderTitle()}
                {this.renderRight()}
            </View>
        );
    }
}

const styles = {
    headerStyle: {
        height: 55,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#243039',
    },
    textStyle: {
        textAlign: 'center',
        color: 'white',
        flexGrow: 1,
        fontSize: 20
    },
    menuLeft: {
        //flexGrow: 1,
        width: 60,
        paddingLeft: 15
    },
    menuRight: {
        width: 60,
        flexGrow: 1,
        paddingRight: 15
    },
    menuRightContent: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-end'
    },
    imgStyle: {       
        marginTop: 11,
        height: 30,
        width: 30
    }
};

export default Header;
