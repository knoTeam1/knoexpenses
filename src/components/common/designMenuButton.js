import React, { Component } from 'react';
import {
    Text,
    TouchableWithoutFeedback,
    TouchableOpacity,
    View,
    Image
  } from 'react-native';

const menuIconsSource = {
  icon1: require('../../../assets/images/icons/icons8-today.png'),
  icon2: require('../../../assets/images/icons/icons8-month-view.png'),
  icon3: require('../../../assets/images/icons/icons8-year-view.png'),
  icon4: require('../../../assets/images/icons/icons8-supplier.png'),
  icon5: require('../../../assets/images/icons/icons8-sign-out.png'),
  icon6: require('../../../assets/images/icons/icons8-slider.png'),
};

 class DsgnMenuButton extends Component {

    renderImage() {
      const { imgStyle } = styles;
      if (this.props.img) {
        return (
            <Image
              style={imgStyle}  
              source={menuIconsSource[this.props.img]}
            /> 
        );
      }
    }

    render() {
      const { containerStyle, textStyle } = styles;
      const { onPress, text } = this.props;
      return (
         <TouchableWithoutFeedback onPress={onPress}>
            <View style={[containerStyle, this.props.buttonStyle]} >
               {this.renderImage()}
                <Text style={[textStyle, this.props.textStyle]}>
                    {text}
                </Text>
            </View>
         </TouchableWithoutFeedback>
      );
    }
  }
  
  const styles = {
    containerStyle: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      borderWidth: 1,
      borderColor: '#3A4F54',
      backgroundColor: '#253039',
    },
    textStyle: {
      color: '#ffffff',
    },
    imgStyle: {       
      height: 40,
      width: 40
    }
  };
  
  export default DsgnMenuButton;
