import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import {
    Text,
    TouchableHighlight,
    View,
    Image
  } from 'react-native';

class TitleHeader extends Component {
    
    // renderLeft() {
    //     const { imgStyle, menuLeft } = styles;
    //     if (this.props.back) {
    //         return (
    //             <TouchableHighlight onPress={() => Actions.pop({ type: 'reset ' })}>
    //                 <View style={menuLeft}>
    //                     <Image
    //                         style={imgStyle}  
    //                         source={require('../../../assets/images/icons/icons8-simple-arrow.png')}
    //                     /> 
    //                 </View>
    //             </TouchableHighlight>
    //         );
    //     }
    // }

    // renderTitle() {
    //     const { textStyle } = styles;
    //     return (
    //         <Text style={textStyle}>{this.props.title}</Text>
    //     );
    // }

    // renderRight() {
    //     const { imgStyle, menuRight, menuRightContent } = styles;   
    //     if (this.props.add) {
    //         return (
    //             <TouchableHighlight onPress={this.onAddPress}>
    //               <View style={menuRight}>
    //                 <View style={menuRightContent}>
    //                     <Image
    //                         style={imgStyle}  
    //                         source={require('../../../assets/images/icons/icons8-add.png')}
    //                     />
    //                 </View>
    //               </View> 
    //              </TouchableHighlight>
    //         );
    //     }
    // }

    render() {
        const { headerStyle, monthStyle, amountStyle } = styles;
        return (
            <View style={headerStyle}>
                <Text style={monthStyle}>{this.props.month}</Text>
                <Text style={amountStyle}>{this.props.amount} </Text>
            </View>
        );
    }
}

const styles = {
    headerStyle: {
        height: 170,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#283946',
    },
    monthStyle: {
        textAlign: 'center',
        textAlignVertical: 'center',
        color: '#00ac8e',
        // borderWidth: 1,
        // borderColor: 'red',
        flexGrow: 1,
        fontWeight: '100',
        fontSize: 32
    },
    amountStyle: {
        textAlign: 'center',
        textAlignVertical: 'center',
        fontWeight: '100',
        fontSize: 48,
        color: '#fd6d38',
        flexGrow: 1,
        // borderWidth: 1,
        // borderColor: 'red',
    },
};

export default TitleHeader;
