import React from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
//import images from '../../../assets/images';

// const images = {
//     menuToday: require('../../../assets/images/menuToday.png'),
//     menuMonth: require('../../../assets/images/menuToday.png'),
//     menuYear: require('../../../assets/images/menuToday.png'),
//     menuProvider: require('../../../assets/images/menuToday.png')
// }


const ImgButton = (props) => {
    const { buttonStyle, textStyle } = styles;
    
    //console.log(images[this.props.imgSource]);

    
    return (
        
        <TouchableOpacity onPress={props.onPress} style={[buttonStyle, props.buttonStyle]}>
        
            {/* <Image
                style={imgStyle}  
                source={images[this.props.imgSource]}
            /> */}
            <Text style={textStyle}> 
                {props.children}
            </Text>
        
        </TouchableOpacity>
    );
};

const styles = {

    container: {
        //flex: 1,
        //backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },

    imgStyle: {       
        alignSelf: 'center',
        height: 50,
        width: 50
    },
    textStyle: {
        //alignSelf: 'center',
        color: '#ffffff',
        fontSize: 14,
        fontWeight: '600',
        paddingTop: 5,
        paddingBottom: 5

    },
    buttonStyle: {
        //display: 'flex',
        //flex: 1,
//        alignSelf: 'center',
//        backgroundColor: '#fff',
//        borderRadius: 5,
//        marginLeft: 5,
//        marginRight: 5,
        alignItems: 'center',
       // alignContent: 'center',
        justifyContent: 'center',
        //alignContent: 'center',
        padding: 10,
        width: 125,
        height: 125,
        borderWidth: 2,
        
        borderColor: '#ffffff', 
    },
    menuContainer: {
        
        display: 'flex',
        flex: 1,
        flexDirection: 'row', 
        flexWrap: 'wrap', 
        // padding: 0,
        // margin: 0
        justifyContent: 'space-around',
        alignContent: 'space-around',
        backgroundColor: '#d84949'
    }
};

export { ImgButton };
