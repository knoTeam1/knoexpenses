import React from 'react';
import { TextInput, View, Text } from 'react-native';

const LoginInput = ({ label, value, onChangeText, placeholder, secure }) => {
    const { inputStyle, labelStyle, containerStyle } = styles;

    return (
        <View style={containerStyle}>
           
            <TextInput
                secureTextEntry={secure}
                autoCorrect={false}
                underlineColorAndroid='transparent'
                placeholder={placeholder}
                value={value}
                onChangeText={onChangeText}
                style={inputStyle}
            />
        </View>
    );    
};

const styles = {
    inputStyle: {
        color: '#FFF',
        paddingRight: 5,
        paddingLeft: 5,
        fontSize: 16,
        // underlineColorAndroid: 'transparent',
        // lineHeight: 23,
        textAlign: 'center',
        flexGrow: 1
    },
    containerStyle: {
        height: 50,
        //flex: 1,
        flexDirection: 'row',
        marginBottom: 10,
        //alignItems: 'center',
        //justifyContent: 'center',
        backgroundColor: '#273947'
        // borderColor: '#34ab93',
        // borderWidth: 1,
        // borderLeftWidth: 0,
        // borderRightWidth: 0
    }
};

export { LoginInput };
