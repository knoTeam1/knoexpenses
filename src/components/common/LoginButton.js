import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const LoginButton = ({ onPress, children }) => {
    const { buttonStyle, textStyle } = styles;

    return (
        <TouchableOpacity onPress={onPress} style={buttonStyle}>
            <Text style={textStyle}> 
                {children}
            </Text>
        </TouchableOpacity>
    );
};

const styles = {
    textStyle: {
        alignSelf: 'center',
        textAlign: 'center',
        flexGrow: 1,
        color: '#ffffff',
        fontSize: 16,
        // fontWeight: '600',
        // paddingTop: 10,
        // paddingBottom: 10

    },
    buttonStyle: {
        flexDirection: 'row',
        height: 50,
        // alignSelf: 'stretch',
        backgroundColor: '#fe5d42',
        // borderColor: '#34ab93',
        // borderWidth: 1,
        // borderLeftWidth: 0,
        // borderRightWidth: 0,
        
    }
};

export { LoginButton };
