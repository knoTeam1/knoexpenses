import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Container, Content, Button, Left, Header, Icon, 
    Body, Title, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { 
    expensesClearData,
    expensesDataUpdate,
    expensesSave,
} from '../actions';
import { Card, CardSection } from './common';

import ExpensesForm from './ExpensesForm';

class ExpensesCreateForm extends Component {

    componentWillMount() {
       //console.log('create form...');
       this.props.expensesClearData();
       console.log(this.props);
       // _.each(this.props.expense, (value, props) => {
            //console.log({ props, value });
       //     this.props.expensesDataUpdate({ props, value });
       // });
    }

    onExpensesSave() {
        const { name, amount, day, month, provider } = this.props;
        console.log('expenses save');
        console.log(this.props);
        this.props.expensesSave({ name, amount, day, month, provider });
    }

    render() {
        const { buttonStyle } = styles;
        return (
            <Container>  
                <Header>  
                    <Left>
                        <Button transparent onPress={() => Actions.pop()}>
                            <Icon name='md-arrow-round-back' />
                        </Button>
                    </Left>           
                    <Body>
                        <Title>Nuevo gasto</Title>
                    </Body>
                </Header> 
                <Content>
                    <Card>
                        <ExpensesForm {...this.props} />                  
                        <CardSection>                   
                            <Button primary style={buttonStyle} onPress={this.onExpensesSave.bind(this)}> 
                               <Text>Crear</Text>
                            </Button>
                        </CardSection>
                    </Card> 
                </Content>
            </Container>
            
        );
    }
}

const styles = {
    inputTextStyle: {
        marginLeft: 60
    },
    buttonStyle: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: 40,
        marginRight: 40
    }
};

const mapStateToProps = (state) => {
    const { name, amount, day, month, provider } = state.expense;
    //console.log('FORM');
    //console.log(state.expense);
    return { name, amount, day, month, provider };
};

export default connect(mapStateToProps, { 
    expensesClearData,
    expensesDataUpdate,
    expensesSave
})(ExpensesCreateForm);
