import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { Content, Button,  
    Item, Input, Text, Label, Picker } from 'native-base';
// import { Actions } from 'react-native-router-flux';
import Header from './common/Header';
import { providerDataUpdate, providerSave } from '../actions';

class ProviderCreateForm extends Component {

    onSave() {
        const { name, type } = this.props;
        this.props.providerSave({ name, type });
    }
  
    render() {
        const { buttonStyle, pickerTextStyle } = styles;

        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                        <Header 
                                back 
                                title={'Nuevo proveedor'} 
                        />  
                        <Content>
                        <Item fixedLabel>
                            <Label>Empresa</Label>
                            <Input 
                                value={this.props.name} 
                                onChangeText={value =>
                                    this.props.providerDataUpdate({ prop: 'name', value })} 
                            />
                        </Item>       
                        <Item >
                            <Label >Servicio </Label>
                            <Picker
                                style={{ flex: 1, marginLeft: 60 }}
                                selectedValue={this.props.type}
                                onValueChange={value =>
                                    this.props.providerDataUpdate({ prop: 'type', value })}
                            >
                                <Picker.Item label="Comunidad" value="Comunidad" />
                                <Picker.Item label="Comunicaciones" value="Comunicaciones" />
                                <Picker.Item label="Bancario" value="Bancario" />
                                <Picker.Item label="Impuestos" value="Impuestos" />
                                <Picker.Item label="Formación" value="Formación" />
                                <Picker.Item label="Seguros" value="Seguros" />
                                <Picker.Item label="Suministros" value="Suministros" />
                                <Picker.Item label="Ocio" value="Ocio" />
                            </Picker>      
                        </Item>
                         <Button 
                            primary 
                            style={buttonStyle} 
                            onPress={this.onSave.bind(this)} 
                         > 
                            <Text> Crear </Text>
                         </Button>                         
                </Content>
            </View>
        );
    }
}

const styles = {
    pickerTextStyle: {
        fontSize: 18,
        paddingLeft: 20,
        paddingRight: 40
    },
    buttonStyle: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: 40,
        marginRight: 40
    }
};

const mapStateToProps = (state) => {
    const { name, type } = state.provider;

    return { name, type };
};

export default connect(mapStateToProps, {
    providerDataUpdate,
    providerSave
})(ProviderCreateForm);
