import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import { connect } from 'react-redux';
import { ScrollView, View } from 'react-native';
// import ListItem from './ListItemExpenses';

import { yearExpensesToFetch } from '../actions/';
import { Spinner } from './common/';
import ListItemCustom from './items/listItemCustom';
import Header from './common/Header';
import TitleHeader from './common/TitleHeader';

let yearAmount;

const months = [
    { name: 'Enero', amount: 0 }, 
    { name: 'Febrero', amount: 0 }, 
    { name: 'Marzo', amount: 0 }, 
    { name: 'Abril', amount: 0 }, 
    { name: 'Mayo', amount: 0 }, 
    { name: 'Junio', amount: 0 }, 
    { name: 'Julio', amount: 0 }, 
    { name: 'Agosto', amount: 0 }, 
    { name: 'Septiembre', amount: 0 }, 
    { name: 'Octubre', amount: 0 }, 
    { name: 'Noviembre', amount: 0 }, 
    { name: 'Diciembre', amount: 0 }
];


class ExpensesListForm extends Component {

    state = { currYear: '0', 
              dataLoaded: false 
    } ;

    componentWillMount() {
        // console.log('year willMount');
        this.setState({ dataLoaded: false });
        this.setState({ currYear: new Date().getFullYear() }); 
        this.props.yearExpensesToFetch(this.props.globals.gid, new Date().getFullYear());
        //this.createDataSource(this.props);
    }

    componentWillReceiveProps(nextProps) {
        //nextProps next set of props will be rendrered and
        //this.props is the old set
        //this.createDataSource(nextProps);
        // console.log('year willReceiveProps');

        yearAmount = 0;
        // console.log('year resetMonthsAmount');
        this.resetMonthsAmount();
        nextProps.expenses.forEach((elem) => {
            months[elem.month - 1].amount += parseInt(elem.amount, 10);
            yearAmount += parseInt(elem.amount, 10);
        });   
        this.setState({ dataLoaded: true });
    }

    resetMonthsAmount() {
        months.forEach((elem) => {
            elem.amount = 0;
        });   
    }

    selectYear(text) {
        this.setState({ dataLoaded: false });
        this.setState({ currYear: text }, () => {
            //if (this.state.currYear >= 2017) {
                this.props.yearExpensesToFetch(this.props.globals.gid, this.state.currYear);  
            //}        
        });
    }

   renderMonthsItems() {
        const monthsItems = [];   
        // console.log('year RenderMonths');   
        // console.log(months);   
         months.forEach((month, i) => {
            monthsItems.push(
                <ListItemCustom key={i}
                                text1={month.name}
                                text2={month.amount + ' €'} 
                                onPress={() => { Actions.appMonthExpenses({ showMonth: i + 1, showYear: this.state.currYear }); }}
                />);
         });  
        return monthsItems;
    }

    renderContent() {   
        switch (this.state.dataLoaded) {
            case true:
                return (
                    //Top Panel
                    <View style={{ flex: 1 }}>
                        <TitleHeader month={new Date().getFullYear()} amount={yearAmount + ' €'} />
                        {/* <View style={{ flex: 1 }}> */}
                             <ScrollView>                         
                                 {this.renderMonthsItems()}
                             </ScrollView>
                        {/* </View> */}
                    </View>
                );
            case false:
            default:
                return (<Spinner size="large" />);      
        }    
    }

    render() {
        return (       
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <Header 
                        back 
                        add 
                        onAddPress={() => Actions.appExpensesCreate()}
                        title={'Gastos Anuales'} 
                />  
                {this.renderContent()}
            </View>
        );
    }
}

const mapStateToProps = state => {
    //CONVERT state.providers object to employees array, expected
    //by datasource
    //console.log(state.providers);
    // console.log('maptopropsYearExpenses');
    // console.log(state.expenses);
    
    let expenses = _.map(state.expenses, (val, uid) => {
        return { ...val, uid };
    });
    
     expenses = expenses.sort((a, b) => a['month'] - b['month'] || a['day'] - b['day']);
     //expenses = expenses.sort(orderByProperty('month', 'day'));
     //console.log('expenses list form');
     //console.log(state.expenses);
     //console.log('expenses sorted');
     //console.log(expenses);
    const { globals } = state;
    return { expenses, globals };
};

export default connect(mapStateToProps, { yearExpensesToFetch })(ExpensesListForm);
