import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Container, Icon, Content, Text, List, ListItem, Left, Body } from 'native-base';

 class SideBar extends Component {
  render() {
    return (
      <Container>
         <Content style={{ backgroundColor: 'white' }}>
         <List>
           <ListItem icon button onPress={() => { Actions.appHome(); }}>
             <Left>
               <Icon name="md-calendar" />
             </Left>
             <Body>
               <Text>Home</Text>
             </Body>
           </ListItem>
           <ListItem icon button onPress={() => { Actions.appMonthExpenses({ showMonth: 0, showYear: 0 }); }}>
             <Left>
               <Icon name="md-calendar" />
             </Left>
             <Body>
               <Text>Mes actual</Text>
             </Body>
           </ListItem>
           <ListItem icon button onPress={() => { Actions.appMonthExpenses({ showMonth: 1, showYear: 2017 }); }}>
             <Left>
               <Icon name="md-calendar" />
             </Left>
             <Body>
               <Text>Enero</Text>
             </Body>
           </ListItem>
           <ListItem icon button onPress={() => { Actions.appExpenses(); }}>
             <Left>
               <Icon name="logo-bitcoin" />
             </Left>
             <Body>
               <Text>Gastos</Text>
             </Body>
           </ListItem>
           <ListItem icon button onPress={() => { Actions.appProviders(); }}>
             <Left>
               <Icon name="cube" />
             </Left>
             <Body>
               <Text>Proveedores</Text>
             </Body>
           </ListItem>
           <ListItem icon button onPress={() => { Actions.auth(); }}>
             <Left>
               <Icon name="person" />
             </Left>
             <Body>
               <Text>Login</Text>
             </Body>
           </ListItem>
         </List>
       </Content>
      </Container>
    );
  }
}

export default SideBar;
