import React, { Component } from 'react';
import { View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import DsgnMenuButton from '../components/common/designMenuButton';
import Header from './common/Header';

class MonthSelector extends Component {

    componentWillMount() {        

    }
    
    render() {   
        const { mainContainer, menuRowStyle, 
            buttonStyle1, buttonStyle2, 
            buttonStyle3, buttonStyle4 
        } = styles;

        const y = new Date().getFullYear();
        //const y = 2017;

        return (       
            <View style={mainContainer}>
                <Header back title={'Selecciona mes'} />  
                    <View style={menuRowStyle}> 
                        <DsgnMenuButton 
                            text='ENERO' 
                            buttonStyle={buttonStyle1} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 1, showYear: y }); }}
                        />
                        <DsgnMenuButton 
                            text='FEBRERO' 
                            buttonStyle={buttonStyle3} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 2, showYear: y }); }} 
                        />
                    </View>
                    <View style={menuRowStyle}> 
                        <DsgnMenuButton 
                            text='MARZO' 
                            buttonStyle={buttonStyle1} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 3, showYear: y }); }} 
                        />
                        <DsgnMenuButton 
                            text='ABRIL' 
                            buttonStyle={buttonStyle3} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 4, showYear: y }); }} 
                        />
                    </View>
                    <View style={menuRowStyle}> 
                        <DsgnMenuButton 
                            text='MAYO' 
                            buttonStyle={buttonStyle1} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 5, showYear: y }); }} 
                        />
                        <DsgnMenuButton 
                            text='JUNIO' 
                            buttonStyle={buttonStyle3} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 6, showYear: y }); }} 
                        />
                    </View>
                    <View style={menuRowStyle}> 
                        <DsgnMenuButton 
                            text='JULIO' 
                            buttonStyle={buttonStyle1} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 7, showYear: y }); }} 
                        />
                        <DsgnMenuButton 
                            text='AGOSTO' 
                            buttonStyle={buttonStyle3} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 8, showYear: y }); }} 
                        />
                    </View>
                    <View style={menuRowStyle}> 
                        <DsgnMenuButton 
                            text='SEPTIEMBRE' 
                            buttonStyle={buttonStyle1} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 9, showYear: y }); }} 
                        />
                        <DsgnMenuButton 
                            text='OCTUBRE' 
                            buttonStyle={buttonStyle3} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 10, showYear: y }); }} 
                        />
                    </View>
                    <View style={menuRowStyle}> 
                        <DsgnMenuButton 
                            text='NOVIEMBRE' 
                            buttonStyle={buttonStyle1} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 11, showYear: y }); }} 
                        />
                        <DsgnMenuButton 
                            text='DICIEMBRE' 
                            buttonStyle={buttonStyle3} 
                            onPress={() => { Actions.appMonthExpenses({ showMonth: 12, showYear: y }); }} 
                        />
                    </View>
        </View>    
        );
    }
}

const styles = {
    mainContainer: {
        flex: 1,
        flexDirection: 'column'
    },
    menuRowStyle: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#273947',
    },   
    buttonStyle1: {
        borderRightWidth: 0,
        borderBottomWidth: 0,
    },
    buttonStyle2: {
        borderWidth: 1,
    },
    buttonStyle3: {
        borderBottomWidth: 0,
    },
    buttonStyle4: {
        borderRightWidth: 0,
    }
};

export default MonthSelector;
