import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import { connect } from 'react-redux';
import { View, ListView } from 'react-native';
import ListItemCustom from './items/listItemCustom';
import { Spinner } from './common/';
import Header from './common/Header';

import { providersFetch } from '../actions/';

class ProvidersForm extends Component {

    state = { dataLoaded: false } ;

    componentWillMount() {
        this.setState({ dataLoaded: false });
        // console.log('providers fetch');
        // console.log(this.props.globals.gid);
        this.props.providersFetch(this.props.globals.gid);
        this.createDataSource(this.props);
    }

    componentWillReceiveProps(nextProps) {
        //nextProps next set of props will be rendrered and
        //this.props is the old set
        this.createDataSource(nextProps);
        this.setState({ dataLoaded: true });
    }

    createDataSource({ providers }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(providers);
    }

    renderRow(provider) {
        return <ListItemCustom text1={provider.name} />;
        // return <ListItem provider={provider} />;
    }

    renderContent() {     
        switch (this.state.dataLoaded) {
            case true:
                return (
                    <ListView
                        enableEmptySections
                        dataSource={this.dataSource}
                        renderRow={this.renderRow}
                    />
                );
            case false:
            default:
                return (<Spinner size="large" />);      
        } 
    }   

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <Header 
                        back 
                        add 
                        onAddPress={() => Actions.appProvidersCreate()}
                        title={'Proveedores'} 
                />  
                {this.renderContent()}
            </View>
        );
    }
}

const mapStateToProps = state => {
    //CONVERT state.providers object to employees array, expected
    //by datasource
    console.log(state.providers);
     const providers = _.map(state.providers, (val, uid) => {
         return { ...val, uid };
     });
     const { globals } = state;
    return { providers, globals };
};

export default connect(mapStateToProps, { providersFetch })(ProvidersForm);
