import React, { Component } from 'react';
import { View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Header from './common/Header';
import YearExpensesList from '../containers/YearExpensesList';

class YearExpensesForm extends Component {

    render() {
        return (       
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <Header 
                        back 
                        add 
                        onAddPress={() => Actions.appExpensesCreate()}
                        title={'Gastos Anuales'} 
                />  
                <YearExpensesList />
            </View>
        );
    }
}

export default YearExpensesForm;
