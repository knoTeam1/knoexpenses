import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, loginUser } from '../actions/AuthActions';

import { LoginInput, Spinner, LoginButton } from './common';

class LoginForm extends Component {
    
    onEmailChange(text) {
        this.props.emailChanged(text);
    }

    onPasswordChange(text) {
        this.props.passwordChanged(text);
    }

    onButtonPress() {
        const { email, password } = this.props;
        this.props.loginUser({ email, password });
    }

    renderError() {
        if (this.props.error) {
            return (
                // <View style={{ backgroundColor: '#243039' }}>
                    <Text style={styles.errorStyle}>
                        {this.props.error}
                    </Text>
                // </View>
            );
        }
    }

    renderButton() {
        if (this.props.loading) {
            return <Spinner size="large" />;
        }

        return (
              <LoginButton onPress={this.onButtonPress.bind(this)}>
                LOGIN                        
              </LoginButton>
        );
    }


    render() {
        const { containerStyle, topStyle, bottomStyle, imgStyle } = styles;

        return (
            <View style={containerStyle}>
                <View style={topStyle}>
                    <Image
                        style={imgStyle}  
                        source={require('../../assets/images/mainLogo.png')}
                    /> 
                </View>
                <View style={bottomStyle}>
                    <LoginInput
                            label="a"
                            placeholder="email@gmail.com"
                            onChangeText={this.onEmailChange.bind(this)}
                            value={this.props.email}
                    />
                    <LoginInput
                            label="a"
                            secure
                            placeholder="password"
                            onChangeText={this.onPasswordChange.bind(this)}
                            value={this.props.password}
                    />        
                    {this.renderButton()}  
                    {this.renderError()}
                </View>
                   
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#243039',
    },
    topStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#243039',
    },
    bottomStyle: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#243039',
    },
    imgStyle: {       
        height: 200,
        width: 200
    },
    errorStyle: {
        color: '#ff6e54',
        fontSize: 16,
        textAlign: 'center',
        marginTop: 20,
        flexGrow: 1
    }
};

const mapStateToProps = ({ auth }) => {
    const { email, password, error, loading } = auth;
    return { email, password, error, loading };
};

export default connect(mapStateToProps, { 
    emailChanged, 
    passwordChanged,
    loginUser,
})(LoginForm);
