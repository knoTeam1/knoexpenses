import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Confirm } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import { Container, Header, Body, Content, Text, 
    Left, Button, Icon, Title } from 'native-base';
import ExpensesForm from './ExpensesForm';
import { 
    expensesDelete,
    expensesDataUpdate,
    expensesEdit
} from '../actions';
import { Card, CardSection } from './common';

class ExpensesEditForm extends Component {
        state = { showModal: false }    
        
        componentWillMount() {
            //console.log('edit will mount');
            _.each(this.props.expense, (value, prop) => {
                //console.log({ props, value });
                this.props.expensesDataUpdate({ prop, value });
            });            
        }
    
        onEditPress() {
            const { name, amount, day, month, provider } = this.props;
            this.props.expensesEdit({ name, amount, day, month, provider, eid: this.props.expense.uid });
        }
    
        onDeletePress() {
            this.setState({ showModal: !this.state.showModal });        
        }
        
        onDecline() {
            this.setState({ showModal: false });        
        }
    
        onAccept() {
            this.props.expensesDelete({ eid: this.props.expense.uid });
            //this.setState({ showModal: false });  
        }
    
    render() {
            const { buttonStyle, headerStyle } = styles;
            return (
                <Container>  
                    <Header style={headerStyle}>  
                        <Left>
                            <Button transparent onPress={() => Actions.pop({ type: 'reset ' })}>
                                <Icon name='md-arrow-round-back' />
                            </Button>
                        </Left>           
                        <Body>
                            <Title>Editar gasto</Title>
                        </Body>
                    </Header> 
                    <Content>
                        <Card>
                            <ExpensesForm {...this.props} />                                                                      
                            <CardSection>                   
                                <Button primary style={buttonStyle} onPress={this.onEditPress.bind(this)}> 
                                   <Text>Guardar</Text>
                                </Button>
                            </CardSection>      
                            <CardSection>                   
                                <Button primary style={buttonStyle} onPress={this.onAccept.bind(this)}> 
                                   <Text>Borrar</Text>
                                </Button>
                            </CardSection>  
                        </Card> 
                    </Content>
                </Container>
                
            );
    }
}

const styles = {
    headerStyle: {
        backgroundColor: '#000000'
    },
    inputTextStyle: {
        marginLeft: 60
    },
    buttonStyle: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: 40,
        marginRight: 40
    }
};

const mapStateToProps = (state) => {
    const { name, amount, day, month, provider } = state.expense;
    //console.log(state.expense);
    return { name, amount, day, month, provider };
};

export default connect(mapStateToProps, {
    expensesDelete,
    expensesDataUpdate,
    expensesEdit
})(ExpensesEditForm);
