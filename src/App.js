import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './Router';


class App extends Component {

    componentWillMount() {
        // Initialize Firebase
        const config = {
          apiKey: 'AIzaSyAJa1FE1ATj74V8WUx10VqQN99D46UDTZc',
          authDomain: 'knoexpenses.firebaseapp.com',
          databaseURL: 'https://knoexpenses.firebaseio.com',
          projectId: 'knoexpenses',
          storageBucket: 'knoexpenses.appspot.com',
          messagingSenderId: '425823232939'
        };
        firebase.initializeApp(config);
    }

    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
        return (
            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}

export default App;
