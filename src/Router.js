import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import HomeForm from './components/HomeForm';
import MonthForm from './components/MonthlyExpensesForm';
import ProvidersForm from './components/ProvidersForm';
import ProviderCreateForm from './components/ProviderCreateForm';
import ExpensesListForm from './components/ExpensesListForm';
import ExpensesCreateForm from './components/ExpensesCreateForm';
import ExpensesEditForm from './components/ExpensesEditForm';
import MonthSelector from './components/MonthSelector';

const RouterComponent = () => {
    return (   
        <Router>
            <Scene key="root" hideNavBar>
                    <Scene key="main" hideNavBar>
                        <Scene 
                            key="appHome" 
                            component={HomeForm} 
                            title="Expenses" 
                        />
                        <Scene 
                            key="appMonthExpenses" 
                            component={MonthForm} 
                            title="Month Expenses" 
                        />
                        <Scene 
                            key="appProviders" 
                            component={ProvidersForm} 
                            title="Providers" 
                        />
                        <Scene 
                            key="appProvidersCreate" 
                            component={ProviderCreateForm} 
                            title="Providers Create" 
                        />
                        <Scene 
                            key="appExpenses" 
                            component={ExpensesListForm} 
                            title="Expenses" 
                        />
                        <Scene 
                            key="appExpensesCreate" 
                            component={ExpensesCreateForm} 
                            title="Expenses Create" 
                        />
                        <Scene 
                            key="appExpensesEdit" 
                            component={ExpensesEditForm} 
                            title="Expenses Edit" 
                        />
                        <Scene 
                            key="appMonthSelector" 
                            component={MonthSelector} 
                            title="Month selector" 
                        />
                    </Scene>
                    <Scene key="auth" initial hideNavBar> 
                        <Scene 
                            key="appLogin" 
                            component={LoginForm} 
                            title="Please Login" 
                        />
                    </Scene>                     
                </Scene>
        </Router>

    );
};

export default RouterComponent;
