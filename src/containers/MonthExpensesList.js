import React, { Component } from 'react';
import { View, ListView } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import ListItemExpensesDetail from '../components/items/ListItemExpensesDetail';
import { Spinner } from '../components/common/Spinner';
import TitleHeader from '../components/common/TitleHeader';
import expensesSelector from '../selectors/expensesSelector';
import { 
    monthExpensesToFetch,
    providersFetch 
} from '../actions/';

let currMonth;
const month = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 
                'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

class MonthExpensesList extends Component {

    state = { dataLoaded: false } ;

    componentWillMount() {        
        let m;
        let y;
        const hasMonth = _.has(Actions.currentParams, 'showMonth');
        const hasYear = _.has(Actions.currentParams, 'showYear');
        this.setState({ dataLoaded: false });
        this.props.providersFetch(this.props.globals.gid);
        // console.log('monthsWillMount');
        if (hasMonth && hasYear) {
            m = Actions.currentParams.showMonth;    
            y = Actions.currentParams.showYear;
            if (m === 0 && y === 0) {
                m = new Date().getMonth();
                y = new Date().getFullYear();
                //y = 2017;
                currMonth = month[m];     
                m += 1;
            } else {
                currMonth = month[m - 1];  
            }

            this.props.monthExpensesToFetch(this.props.globals.gid, String(y), String(m));
            this.createDataSource(this.props);
        } 
    }
    
    componentWillReceiveProps(nextProps) {
        this.createDataSource(nextProps);
        this.setState({ dataLoaded: true });
    }

    getProviderName(pId) {
        return this.props.providers[pId].name;
    }
    
    parseNumToLocaleStr(num) {
        if (num === undefined) return;
        return num.toLocaleString(undefined, { maximumFractionDigits: 2 }) + ' €';
    }

    createDataSource({ expenses }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(expenses);
    }

    renderHomeRow(expense) {     
        let pName;
        if (this.props.providers[expense.provider] === undefined) {
            pName = '';
        } else {
            pName = this.props.providers[expense.provider].name;
        }   

        return (
            <ListItemExpensesDetail 
                provider={expense} 
                providerName={pName} 
            />
        );
    }

    renderContent() {     
        switch (this.state.dataLoaded) {
            case true:
                return (
                        //Top Panelamount={`${this.props.yearAmount} €`} />
                        <View style={{ flex: 1 }}>
                            <TitleHeader month={currMonth} amount={this.parseNumToLocaleStr(this.props.totalAmount)} />
                            <ListView
                                enableEmptySections
                                dataSource={this.dataSource}
                                renderRow={this.renderHomeRow.bind(this)}
                            />
                        </View>
                );
            case false:
            default:
                return (<Spinner size="large" />);      
        }    
    }

    render() {              
        return (       
                this.renderContent()    
        );
    }
}

const mapStateToProps = state => {
    const { globals, providers } = state;
    return {  
        globals,
        providers,
        expenses: expensesSelector(state).expensesSort,
        totalAmount: expensesSelector(state).totalMonth,
    };
};

export default connect(mapStateToProps, { 
        monthExpensesToFetch, 
        providersFetch 
    }
)(MonthExpensesList);
