import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { ScrollView, View } from 'react-native';
import expensesSelector from '../selectors/monthsSelector';
import { yearExpensesToFetch } from '../actions/';
import { Spinner } from '../components/common/';
import ListItemCustom from '../components/items/listItemCustom';
import TitleHeader from '../components/common/TitleHeader';

class YearExpensesList extends Component {

    state = { currYear: '0', 
              dataLoaded: false 
    } ;

    componentWillMount() {
        // console.log('year willMount');
        this.setState({ dataLoaded: false });
        this.setState({ currYear: new Date().getFullYear() }); 
        this.props.yearExpensesToFetch(this.props.globals.gid, new Date().getFullYear());
    }

    componentWillReceiveProps() {
        this.setState({ dataLoaded: true });
    }

    selectYear(text) {
        this.setState({ dataLoaded: false });
        this.setState({ currYear: text }, () => {
            //if (this.state.currYear >= 2017) {
                this.props.yearExpensesToFetch(this.props.globals.gid, this.state.currYear);  
            //}        
        });
    }

   renderMonthsItems() {
        const monthsItems = [];   
         this.props.months.forEach((month, i) => {
            monthsItems.push(
                <ListItemCustom 
                                key={i}
                                text1={month.name}
                                text2={`${month.amount} €`} 
                                rightIcon
                                onPress={() => { 
                                    Actions.appMonthExpenses({ showMonth: i + 1, showYear: this.state.currYear }); 
                                }}
                />);
         });  
        return monthsItems;
    }

    renderContent() {   
        switch (this.state.dataLoaded) {
            case true:
                return (
                    //Top Panel
                    <View style={{ flex: 1 }}>
                        <TitleHeader month={new Date().getFullYear()} amount={`${this.props.yearAmount} €`} />
                        {/* <View style={{ flex: 1 }}> */}
                             <ScrollView>                         
                                 {this.renderMonthsItems()}
                             </ScrollView>
                        {/* </View> */}
                    </View>
                );
            case false:
            default:
                return (<Spinner size="large" />);      
        }    
    }

    render() {
        return (       
            this.renderContent()            
        );
    }
}

const mapStateToProps = state => {
    const { globals } = state;
    return {  
        globals,
        months: expensesSelector(state).months,
        yearAmount: expensesSelector(state).yearAmount,
    };
};

export default connect(mapStateToProps, { yearExpensesToFetch })(YearExpensesList);
