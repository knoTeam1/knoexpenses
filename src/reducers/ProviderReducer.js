import {
    PR_DATA_UPDATE,
    PR_EDIT,
    PR_SAVE
} from '../actions/types';

const INITIAL_STATE = {
    name: '',
    type: 'Comunidad'
};

export default (state = INITIAL_STATE, action) => {
    //console.log(action.payload);
    switch (action.type) {
        case PR_DATA_UPDATE:
            return { ...state, [action.payload.prop]: action.payload.value };
        case PR_EDIT:
            return INITIAL_STATE;
        case PR_SAVE:
            return INITIAL_STATE;
        default:
            return state;
    }
};
