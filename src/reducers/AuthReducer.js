import { 
    EMAIL_CHANGED, 
    PASSWORD_CHANGED,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    LOGIN_USER,
    LOGOUT_USER
} from '../actions/types';

const INITIAL_STATE = { 
    email: 'test@test.com', 
    password: '123456',
    user: null,
    error: '',
    loading: false,
};

function checkLoginError(errCode) {
    let errDesc = '';
    if (errCode === 'auth/wrong-password') {
        errDesc = 'Contraseña incorrecta';
    } else if (errCode === 'auth/invalid-email') {
        errDesc = 'E-mail incorrecto';
    } else if (errCode === 'auth/user-disabled') {
        errDesc = 'Usuario deshabilitado';
    } else if (errCode === 'auth/user-not-found') {
        errDesc = 'Usuario no encontrado';
    }
    return errDesc;
}

export default (state = INITIAL_STATE, action) => {
    //console.log(action);
    let errC;
    switch (action.type) {
 
        case EMAIL_CHANGED:
            return { ...state, email: action.payload };  
        case PASSWORD_CHANGED:
            return { ...state, password: action.payload };  
        case LOGIN_USER:
            return { ...state, loading: true, error: '' };  
        case LOGOUT_USER:
            return { ...state, loading: false, error: '' };  
        case LOGIN_USER_SUCCESS:
            return { ...state, ...INITIAL_STATE, user: action.payload };  
        case LOGIN_USER_FAIL:
            errC = checkLoginError(action.payload);
            return { ...state, 
                error: errC, 
                password: '', 
                loading: false 
            };  
        default:        
            return state;
    }
};
