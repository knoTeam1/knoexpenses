import { 
    GLOBALS_SET_GID
} from '../actions/types';

const INITIAL_STATE = { 
    gid: '-1',
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GLOBALS_SET_GID:
            // console.log('global reducer');
            console.log(action);
            return { ...state, gid: action.payload }; 
        default:        
            return state;
    }
};
