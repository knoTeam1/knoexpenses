import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import ProviderListReducer from './ProviderListReducer';
import ProviderReducer from './ProviderReducer';
import ExpensesListReducer from './ExpensesListReducer';
import ExpensesReducer from './ExpensesReducer';
import MonthExpensesReducer from './MonthExpensesListReducer';
import GlobalsReducer from './GlobalsReducer';

export default combineReducers({
    auth: AuthReducer,
    providers: ProviderListReducer,
    provider: ProviderReducer,
    expenses: ExpensesListReducer,
    expense: ExpensesReducer,
    monthExpenses: MonthExpensesReducer,
    globals: GlobalsReducer
});
