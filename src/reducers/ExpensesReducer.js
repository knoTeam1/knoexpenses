import { 
    EXPENSES_DATA_UPDATE,
    EXPENSES_SAVE,
    EXPENSES_EDIT,
    EXPENSES_CLEAR_DATA,
    EXPENSES_DELETE
} from '../actions/types';

const INITIAL_STATE = {
    name: '',
    amount: '',
    day: '',
    month: '1',
    provider: '-L7iKca6-OMfoxMaEMug'
};

export default (state = INITIAL_STATE, action) => {
    //console.log('expensesreducer');
    switch (action.type) {
        case EXPENSES_DATA_UPDATE:
            console.log(action.payload);
            return { ...state, [action.payload.prop]: action.payload.value };
        case EXPENSES_CLEAR_DATA:
            return INITIAL_STATE;
        case EXPENSES_EDIT:
            return INITIAL_STATE;
        case EXPENSES_SAVE:
            return INITIAL_STATE;      
        case EXPENSES_DELETE:
            return INITIAL_STATE;      
        default:        
            return state;
    }
};
